#!make

BUILD_CACHE_PREFIX=""
PROXY_CACHE_PREFIX=""
VERSION="8.4"

.PHONY: help

help: ## Show this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\033[36m\033[0m\n"} /^[$$()% a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

update: ## Update Dockerfiles based on config
	go mod tidy -v
	go get -v -t -u ./...
	go mod vendor -v
	go run update.go

.PHONY: php-basic
php-basic:
	docker build --file dist/$(VERSION)/Dockerfile.basic --tag $(BUILD_CACHE_PREFIX)php-basic:$(VERSION) --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" dist/$(VERSION)

.PHONY: php-build-fpm
php-build-fpm:
	docker build --file dist/$(VERSION)/fpm/Dockerfile.build --tag $(BUILD_CACHE_PREFIX)php-build-fpm:$(VERSION) --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" --build-arg "BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX)" dist/$(VERSION)/fpm

.PHONY: php-additional-fpm-full
php-additional-fpm-full:
	docker build --file dist/$(VERSION)/fpm/Dockerfile.additional-full --tag $(BUILD_CACHE_PREFIX)php-additional-fpm-full:$(VERSION) --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" --build-arg "BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX)" dist/$(VERSION)/fpm

.PHONY: php-additional-fpm-std
php-additional-fpm-std:
	docker build --file dist/$(VERSION)/fpm/Dockerfile.additional-std --tag $(BUILD_CACHE_PREFIX)php-additional-fpm-std:$(VERSION) --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" --build-arg "BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX)" dist/$(VERSION)/fpm

.PHONY: php-combined-nginx-full
php-combined-nginx-full:
	docker build --file dist/$(VERSION)/nginx/Dockerfile.combined-full --tag $(BUILD_CACHE_PREFIX)php-combined-nginx-full:$(VERSION) --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" --build-arg "BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX)" dist/$(VERSION)/nginx

.PHONY: nginx-alpine3.16
nginx-alpine3.16:
	docker build --file nginx/alpine3.16/Dockerfile --tag $(BUILD_CACHE_PREFIX)php-nginx:alpine3.16 --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" nginx/alpine3.16

.PHONY: nginx-alpine3.18
nginx-alpine3.18:
	docker build --file nginx/alpine3.18/Dockerfile --tag $(BUILD_CACHE_PREFIX)php-nginx:alpine3.18 --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" nginx/alpine3.18

.PHONY: nginx-alpine3.20
nginx-alpine3.20:
	docker build --file nginx/alpine3.20/Dockerfile --tag $(BUILD_CACHE_PREFIX)php-nginx:alpine3.20 --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" nginx/alpine3.20

.PHONY: all
all: update nginx-alpine3.20 php-basic php-build-fpm php-additional-fpm-full php-combined-nginx-full