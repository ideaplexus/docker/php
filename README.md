# ideaplexus / php

Since PHP is widely used, I wanted to offer docker containers that come with a list of pre-installed plugins and tools, unlike the original containers.
I also wanted to offer containers not only for the latest PHP versions, but also for older versions.

The result is this repository which contains the code for the Docker blueprints for PHP.
The Docker container can be found [here](https://hub.docker.com/r/ideaplexus/php).

## Directories

### dist

Contains the source code for each container. All code/data are generated/copied from `src` directory.

### src

Contains all files and scripts to generate the container in `dist` directory.

## old

The `old` repository contains the old generated files of all variants, but there were a lot of updates in alpine, not all work jet.
I'm working on it to get them work again.

## HowTo update

Update the `config.yaml`, then call `npm install --frozen-lockfile` and then `npm run render`.