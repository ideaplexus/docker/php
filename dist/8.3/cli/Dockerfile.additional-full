ARG BUILD_CACHE_PREFIX

FROM ${BUILD_CACHE_PREFIX}php-build-cli:8.3

COPY extensions/* /usr/local/bin/

RUN set -eux; \
  docker-php-ext-install-apcu; \
  docker-php-ext-install-bcmath; \
  docker-php-ext-install-bz2; \
  docker-php-ext-install-dba; \
  docker-php-ext-install-exif; \
  docker-php-ext-install-gd; \
  docker-php-ext-install-gettext; \
  docker-php-ext-install-gmp; \
  docker-php-ext-install-grpc; \
  docker-php-ext-install-igbinary; \
  docker-php-ext-install-imagick; \
  docker-php-ext-install-imap; \
  docker-php-ext-install-intl; \
  docker-php-ext-install-ldap; \
  docker-php-ext-install-memcached; \
  docker-php-ext-install-mongodb; \
  docker-php-ext-install-msgpack; \
  docker-php-ext-install-mysql; \
  docker-php-ext-install-opcache; \
  docker-php-ext-install-pcntl; \
  docker-php-ext-install-pgsql; \
  docker-php-ext-install-pthreads; \
  docker-php-ext-install-redis; \
  docker-php-ext-install-snmp; \
  docker-php-ext-install-soap; \
  docker-php-ext-install-sockets; \
  docker-php-ext-install-sodium; \
  docker-php-ext-install-sqlsrv; \
  docker-php-ext-install-xdebug; \
  docker-php-ext-install-xhprof; \
  docker-php-ext-install-xmlrpc; \
  docker-php-ext-install-xsl; \
  docker-php-ext-install-zip;

# sodium was built as a shared module (so that it can be replaced later if so desired), so let's enable it too (https://github.com/docker-library/php/issues/598)
RUN docker-php-ext-enable sodium

COPY tools/* /usr/local/bin/

# ensure all tools are installed
RUN set -eux; \
  COMPOSER_ALLOW_SUPERUSER=1 composer --version; \
  COMPOSER_ALLOW_SUPERUSER=1 phpunit --version; \
  COMPOSER_ALLOW_SUPERUSER=1 ppm --version; \
  COMPOSER_ALLOW_SUPERUSER=1 psalm --version; \
  COMPOSER_ALLOW_SUPERUSER=1 phpstan --version; \
  COMPOSER_ALLOW_SUPERUSER=1 PHP_CS_FIXER_IGNORE_ENV=1 php-cs-fixer --version; \
  COMPOSER_ALLOW_SUPERUSER=1 phpcs --version; \
  COMPOSER_ALLOW_SUPERUSER=1 phpcbf --version; \
  COMPOSER_ALLOW_SUPERUSER=1 phpmd --version; \
  COMPOSER_ALLOW_SUPERUSER=1 box --version