worker_processes auto;

user www-data;

daemon off;

worker_rlimit_nofile 8192;

events {
  worker_connections 8000;
  multi_accept on;
  use epoll;
}

error_log /dev/stderr info;

pid /run/nginx.pid;

http {
  include /etc/nginx/mime.types;
  default_type application/octet-stream;

  log_format main '$remote_addr - $remote_user [$time_local] "$request" '
                  '$status $body_bytes_sent "$http_referer" '
                  '"$http_user_agent" "$http_x_forwarded_for"';

  access_log /dev/stdout main;

  sendfile on;
  tcp_nopush on;
  keepalive_timeout 65;

  gzip on;
  gzip_min_length 256;
  gzip_vary on;

  gzip_types
    application/atom+xml
    application/javascript
    application/json
    application/rss+xml
    application/vnd.ms-fontobject
    application/x-font-ttf
    application/x-web-app-manifest+json
    application/xhtml+xml
    application/xml
    font/opentype
    image/svg+xml
    image/x-icon
    text/css
    text/plain
    text/x-component;

  include /etc/nginx/conf.d/*.conf;
}
