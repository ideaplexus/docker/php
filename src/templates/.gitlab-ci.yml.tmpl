stages:
  - nginx
{{- range .Stages }}
  - "{{ . }}"
{{- end }}

.build-template:
  tags:
    - build
  needs: []
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: ['']
  before_script:
    # https://github.com/GoogleContainerTools/kaniko/issues/1297#issuecomment-1149054291
    - if [ -d "/var" ]; then mv /var /var-orig; fi
    - >
      if [ -f /etc/gitlab-runner/certs/ca.crt ]; then
        export REGISTRY_CERTIFICATE="--registry-certificate=${HARBOR_SERVER}=/etc/gitlab-runner/certs/ca.crt"
      fi
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${HARBOR_SERVER}\":{\"username\":\"${HARBOR_USER}\",\"password\":\"${HARBOR_PASSWORD}\"}}}" > /kaniko/.docker/config.json
  script:
    - /kaniko/executor $REGISTRY_CERTIFICATE --context ${BUILD_PATH} --dockerfile ${DOCKERFILE} ${BUILD_ARG} ${DESTINATION}
{{- range $i, $variant := .Variants }}

"{{ $variant.VersionMajorMinor }}-basic":
  stage: "{{ $variant.VersionMajorMinor }}"
  extends:
    - .build-template
  variables:
    BUILD_PATH: {{ $variant.BuildPath }}
    DOCKERFILE: ${BUILD_PATH}/Dockerfile.basic
    BUILD_ARG: --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/php-basic:{{ $variant.VersionMajorMinor }}
{{- range $j, $buildTarget := $variant.BuildTargets }}

"{{ $variant.VersionMajorMinor }}-build-{{ $buildTarget.Name }}":
  stage: "{{ $variant.VersionMajorMinor }}"
  extends:
    - .build-template
  needs:
    - "{{ $variant.VersionMajorMinor }}-basic"
  variables:
    BUILD_PATH: {{ $variant.BuildPath }}/{{ $buildTarget.Name }}
    DOCKERFILE: ${BUILD_PATH}/Dockerfile.build
    BUILD_ARG: --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/ --build-arg  BUILD_CACHE_PREFIX=${HARBOR_SERVER}/build_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/php-build-{{ $buildTarget.Name }}:{{ $variant.VersionMajorMinor }}
  {{- range $k, $additionalVariant := $.AdditionalVariants }}

"{{ $variant.VersionMajorMinor }}-additional-{{ $buildTarget.Name }}-{{ $additionalVariant }}":
  stage: "{{ $variant.VersionMajorMinor }}"
  extends:
    - .build-template
  needs:
    - "{{ $variant.VersionMajorMinor }}-build-{{ $buildTarget.Name }}"
  variables:
    BUILD_PATH: {{ $variant.BuildPath }}/{{ $buildTarget.Name }}
    DOCKERFILE: ${BUILD_PATH}/Dockerfile.additional-{{ $additionalVariant }}
    BUILD_ARG: --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/ --build-arg  BUILD_CACHE_PREFIX=${HARBOR_SERVER}/build_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/php-additional-{{ $buildTarget.Name }}-{{ $additionalVariant }}:{{ $variant.VersionMajorMinor }}
  {{- end }}
{{- end }}
{{- range $l, $finalTarget := $variant.FinalTargets }}
  {{- range $m, $additionalVariant := $.AdditionalVariants }}

"{{ $variant.VersionMajorMinor }}-{{ $finalTarget.Name }}-{{ $additionalVariant }}":
  stage: "{{ $variant.VersionMajorMinor }}"
  extends:
    - .build-template
  needs:
    - "{{ $variant.VersionMajorMinor }}-additional-{{ if $finalTarget.Parent }}{{ $finalTarget.Parent }}{{ else }}{{ $finalTarget.Name }}{{ end }}-{{ $additionalVariant }}"
  variables:
    BUILD_PATH: {{ $variant.BuildPath }}/{{ $finalTarget.Name }}
    DOCKERFILE: ${BUILD_PATH}/Dockerfile.combined-{{ $additionalVariant }}
    BUILD_ARG: --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/ --build-arg  BUILD_CACHE_PREFIX=${HARBOR_SERVER}/build_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/ideaplexus/php:{{ $variant.VersionMajorMinor }}-{{ $finalTarget.Name }}{{ if not (eq $additionalVariant "std") }}-{{ $additionalVariant }}{{ end }}{{ if eq $.Latest $variant.VersionMajorMinor }} --destination ${HARBOR_SERVER}/ideaplexus/php:latest-{{ $finalTarget.Name }}{{ if not (eq $additionalVariant "std") }}-{{ $additionalVariant }}{{ end }}{{ if and (eq $finalTarget.Name "cli") (eq $additionalVariant "std") }} --destination ${HARBOR_SERVER}/ideaplexus/php:latest{{ end}}{{ end }}
    {{- end }}
  {{- end }}
{{- end }}

nginx-alpine3.16:
  stage: nginx
  extends:
    - .build-template
  needs: []
  variables:
    BUILD_PATH: nginx/alpine3.16
    DOCKERFILE: ${BUILD_PATH}/Dockerfile
    BUILD_ARG: --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/php-nginx:alpine3.16

nginx-alpine3.20:
  stage: nginx
  extends:
    - .build-template
  needs: []
  variables:
    BUILD_PATH: nginx/alpine3.20
    DOCKERFILE: ${BUILD_PATH}/Dockerfile
    BUILD_ARG: --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/php-nginx:alpine3.20