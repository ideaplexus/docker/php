ARG PROXY_CACHE_PREFIX
{{ if lt .VersionMajorMinor "8.1" }}
FROM ${PROXY_CACHE_PREFIX}alpine:3.16
{{ else }}
FROM ${PROXY_CACHE_PREFIX}alpine:3.20
{{- end }}

ENV PHPIZE_DEPS="\
    autoconf \
    dpkg-dev dpkg \
    file \
    g++ \
    gcc \
    libc-dev \
    make \
    pkgconf \
    re2c"

RUN apk add --no-cache \
    ca-certificates \
    curl \
    openssl \
    tar \
    xz

RUN set -eux; \
    adduser -u 82 -D -S -G www-data www-data

ENV PHP_INI_DIR=/usr/local/etc/php

RUN set -eux; \
    mkdir -p "$PHP_INI_DIR/conf.d"; \
    [ ! -d /var/www/html ]; \
    mkdir -p /var/www/html; \
    chown www-data:www-data /var/www/html; \
    chmod 777 /var/www/html

ENV PHP_CFLAGS="-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"
ENV PHP_CPPFLAGS="$PHP_CFLAGS"
ENV PHP_LDFLAGS="-Wl,-O1 -pie"

ENV GPG_KEYS="{{ .GpgKeys }}"

ENV PHP_VERSION="{{ .Version }}"
ENV PHP_URL="{{ .Url }}"
ENV PHP_ASC_URL="{{ .AscUrl }}"
ENV PHP_SHA256="{{ .Sha256 }}"

{{- /* PHP 5.4 is ony available as .bz2 */}}
{{ if eq .VersionMajorMinor "5.4" }}
RUN set -eux; \
    \
    apk add --no-cache --virtual .fetch-deps gnupg bzip2; \
    \
    mkdir -p /usr/src; \
    cd /usr/src; \
    \
    curl -fsSL -o php.tar.bz2 "$PHP_URL"; \
    \
    if [ -n "$PHP_SHA256" ]; then \
        echo "$PHP_SHA256 *php.tar.bz2" | sha256sum -c -; \
    fi; \
    \
    if [ -n "$PHP_ASC_URL" ]; then \
        curl -fsSL -o php.tar.bz2.asc "$PHP_ASC_URL"; \
        export GNUPGHOME="$(mktemp -d)"; \
        for key in $GPG_KEYS; do \
            gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key"; \
        done; \
        gpg --batch --verify php.tar.bz2.asc php.tar.bz2; \
        gpgconf --kill all; \
        rm -rf "$GNUPGHOME"; \
    fi; \
    \
    apk del --no-network .fetch-deps; \
    \
    cd /usr/src; \
    tar xjf php.tar.bz2; \
    rm -f php.tar.bz2; \
    tar cJf php.tar.xz php-$phpVersion; \
    rm -rf php-$phpVersion *.asc
{{ else }}
RUN set -eux; \
    \
    apk add --no-cache --virtual .fetch-deps gnupg; \
    \
    mkdir -p /usr/src; \
    cd /usr/src; \
    \
    curl -fsSL -o php.tar.xz "$PHP_URL"; \
    \
    if [ -n "$PHP_SHA256" ]; then \
        echo "$PHP_SHA256 *php.tar.xz" | sha256sum -c -; \
    fi; \
    \
    if [ -n "$PHP_ASC_URL" ]; then \
        curl -fsSL -o php.tar.xz.asc "$PHP_ASC_URL"; \
        export GNUPGHOME="$(mktemp -d)"; \
        for key in $GPG_KEYS; do \
            gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key"; \
        done; \
        gpg --batch --verify php.tar.xz.asc php.tar.xz; \
        gpgconf --kill all; \
        rm -rf "$GNUPGHOME"; \
    fi; \
    \
    apk del --no-network .fetch-deps
{{- end }}
